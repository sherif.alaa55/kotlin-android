package com.example.myapplication

import org.junit.Test

import org.junit.Assert.*

class SampleTest {

    private val testSample: Sample = Sample()

    @Test
    fun sum() {
        val expected = 42
        assertEquals(expected, testSample.sum(40, 2))
    }
}