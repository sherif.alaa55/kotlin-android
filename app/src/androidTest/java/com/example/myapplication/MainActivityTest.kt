package com.example.myapplication

import android.widget.TextView
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Test
    fun testOnCreate() {
        // Launch the activity
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                // Verify the activity is not null
                assert(activity != null)

                // Add more assertions to verify the onCreate behavior
                // For example, check if a TextView is displayed with the correct text
//                val textView = activity.findViewById<TextView>(R.id.textView)
//                assert(textView.text == "Hello, World!")
            }
        }
    }
}
